import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.*;
import java.io.*;

/**
 * Created by Poko on 24-03-2017.
 */
public class Server extends JFrame implements ActionListener {
    private JPanel panel1;
    private JTextArea textArea1;
    private JTextField textField1;
    private JButton button1;
    static ServerSocket ss;
    static Socket s;
    static DataInputStream din;
    static DataOutputStream dout;

    public Server() {
        //setLocationRelativeTo(null);
        setContentPane(panel1);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        button1.addActionListener(this);
        pack();
        setVisible(true);
        setSize(500, 500);
    }

    public static void main(String[] args) {
        String msgin = "";
        Server a = new Server();
        try {
            ss = new ServerSocket(1201);
            s = ss.accept();

            din = new DataInputStream(s.getInputStream());
            dout = new DataOutputStream(s.getOutputStream());

            while (!msgin.equals("exit")) {
                msgin = din.readUTF();
                a.textArea1.setText( a.textArea1.getText().trim() + "\n Client : \t" + msgin);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void actionPerformed(ActionEvent e) {
       // textField1.setText(" ");
        try {
            String msgout = "";
            msgout = textField1.getText().trim();
            dout.writeUTF(msgout);
            textField1.setText(" ");
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }


}
