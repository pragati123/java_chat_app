import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

/**
 * Created by Poko on 24-03-2017.
 */
public class Client extends JFrame implements ActionListener {
    private JTextArea textAreaC;
    private JPanel panelC;
    private JTextField textFieldC;
    private JButton buttonC;
    static Socket s;
    static DataInputStream din;
    static DataOutputStream dout;


    public Client() {
        setLocationRelativeTo(null);
        setContentPane(panelC);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        buttonC.addActionListener(this);
        pack();
        setVisible(true);
        setSize(500, 500);
    }

    public static void main(String[] args) {
        String msgin = "";
        Client b = new Client();
        try {
            s = new Socket("127.0.0.1", 1201);
            din = new DataInputStream(s.getInputStream());
            dout = new DataOutputStream(s.getOutputStream());
            while (!msgin.equals("exit")) {
                msgin = din.readUTF();
                b.textAreaC.setText( b.textAreaC.getText().trim() + "\n Server : \t" + msgin);

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //textFieldC.setText("");

        try {
            String msgout = "";
            msgout = textFieldC.getText().trim();
            dout.writeUTF(msgout);
            textFieldC.setText(" ");
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

}

